from django.db import models
from recipes.models import USER_MODEL

# Create your models here.


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField(null=True)
    owner = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="mealplans")

    def __str__(self):
        owner_name = str(self.owner)
        return self.name + " by " + owner_name
