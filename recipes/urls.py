from django.urls import path


from recipes.views import (
    RecipeCreateView,
    RecipeDeleteView,
    RecipeUpdateView,
    log_rating,
    RecipeDetailView,
    RecipeListView,
    UserListView,
    create_shopping_item,
    ShoppingListListView,
    delete_everything_from_shopping_list
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("userlist/", UserListView.as_view(), name="users_list"),
    path(
        "shopping_list/create/",
        create_shopping_item,
        name="shopping_list_create",
    ),
    path(
        "shopping_list/", ShoppingListListView.as_view(), name='shopping_list_list'),
    path(
        "shopping_list/delete.", delete_everything_from_shopping_list, name='shopping_list_delete_all'),
]
