from django import template

register = template.Library()

  # Get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.servings
    # properties

    # If the servings from the recipe is not None
    #   and the value of target is not None
        # try
            # calculate the ratio of target over
            #   servings
            # return the ratio multiplied by the
            #   ingredient's amount
        # catch a possible error
            # pass
    # return the original ingredient's amount since
    #   nothing else worked

def resize_to(ingredient, target):
    origin_serving = ingredient.recipe.servings
    if origin_serving and target:
        try:
            ratio = int(target) / origin_serving
            return ingredient.amount * ratio
        except ValueError:
            pass
    return ingredient.amount

register.filter(resize_to)